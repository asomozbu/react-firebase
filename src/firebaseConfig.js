export default {
    apiKey: "AIzaSyBxqzLxGxTUIwvXIhw9Yv1wVKXDhRmPdvE",
    authDomain: "fir-workshop-1148b.firebaseapp.com",
    databaseURL: "https://fir-workshop-1148b.firebaseio.com",
    projectId: "fir-workshop-1148b",
    storageBucket: "fir-workshop-1148b.appspot.com",
    messagingSenderId: "1056240622137"
}

export const dbFirebaseConfig = {
    userProfile: 'users', // firebase root where user profiles are stored
    enableLogging: false, // enable/disable Firebase's database logging
};

export const authFirebaseConf = {
    email: {
        verifyOnSignup: false, // Sends verification email to user upon sign up
        saveUserInDatabase: true // Saves user in database at /users ref
    },
    google: {
        // redirect: true, // Opens a pop up by default
        returnAccessToken: true, // Returns an access token as googleAccessToken prop
        saveUserInDatabase: true // Saves user in database at /users ref
    },
    facebook: {
        // redirect: true, // Opens a pop up by default
        returnAccessToken: true, // Returns an access token as googleAccessToken prop
        saveUserInDatabase: true // Saves user in database at /users ref
    },
    github: {
        // redirect: true,
        returnAccessToken: true,
        saveUserInDatabase: true
    },
    twitter: {
        // redirect: true,
        returnAccessToken: true,
        returnSecret: true,
        saveUserInDatabase: true
    }
};