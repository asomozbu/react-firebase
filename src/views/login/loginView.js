import React from "react";
import withFirebaseAuth from "react-auth-firebase";
import firebase from '../../firebaseInit';
import {authFirebaseConf} from '../../firebaseConfig';

const handleSubmit = (ev) => {
    ev.preventDefault();
};

const SignUpView = (props) => {
    const {
        signInWithEmail,
        signUpWithEmail,
        signInWithGoogle,
        signInWithFacebook,
        signInWithGithub,
        signInWithTwitter,
        googleAccessToken,
        facebookAccessToken,
        githubAccessToken,
        twitterAccessToken,
        twitterSecret,
        signOut,
        user,
        error,
    } = props;

    return (
        <div>
            <h1>Sign up</h1>
            <form onSubmit={handleSubmit}>
                <label>
                    Email
                    <input
                        name="email"
                        type="email"
                        placeholder="Email"
                    />
                </label>
                <label>
                    Password
                    <input
                        name="password"
                        type="password"
                        placeholder="Password"
                    />
                </label>
                <button type="submit">Sign Up</button>
            </form>
            <button onClick={signInWithGoogle}>Signin with Google</button>
            <button onClick={signInWithFacebook}>Signin with Facebook</button>
            <button onClick={signInWithGithub}>Signin with Github</button>
            <button onClick={signInWithTwitter}>Signin with Twitter</button>
        </div>
    );
};

export default withFirebaseAuth(SignUpView, firebase, authFirebaseConf);